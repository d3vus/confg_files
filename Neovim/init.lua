--[[
        _       _                        _   _               
  _ __ (_)_ __ | | __  _ __   __ _ _ __ | |_| |__   ___ _ __ 
 | '_ \| | '_ \| |/ / | '_ \ / _` | '_ \| __| '_ \ / _ \ '__|
 | |_) | | | | |   <  | |_) | (_| | | | | |_| | | |  __/ |   
 | .__/|_|_| |_|_|\_\ | .__/ \__,_|_| |_|\__|_| |_|\___|_|   
 |_|                  |_|                                 


   .--.            .--.
 ( (`\\."--``--".//`) )
  '-.   __   __    .-'
   /   /__\ /__\   \
  |    \ 0/ \ 0/    |
  \     `/   \`     /
   `-.  /-"""-\  .-`
     /  '.___.'  \
     \     I     /
      `;--'`'--;`
        '.___.'
--]]                                                                                                                                              

-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--[[
__      __     _____  _____          ____  _      ______  _____ 
 \ \    / /\   |  __ \|_   _|   /\   |  _ \| |    |  ____|/ ____|
  \ \  / /  \  | |__) | | |    /  \  | |_) | |    | |__  | (___  
   \ \/ / /\ \ |  _  /  | |   / /\ \ |  _ <| |    |  __|  \___ \ 
    \  / ____ \| | \ \ _| |_ / ____ \| |_) | |____| |____ ____) |
     \/_/    \_\_|  \_\_____/_/    \_\____/|______|______|_____/ 

]]
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

local set = vim.opt
local global = vim.g
local keymap = vim.keymap
local cmd = vim.cmd


----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--[[
____   _____  _______  _____  ____   _   _   _____ 
  / __ \ |  __ \|__   __||_   _|/ __ \ | \ | | / ____|
 | |  | || |__) |  | |     | | | |  | ||  \| || (___  
 | |  | ||  ___/   | |     | | | |  | || . ` | \___ \ 
 | |__| || |       | |    _| |_| |__| || |\  | ____) |
  \____/ |_|       |_|   |_____|\____/ |_| \_||_____/ 
  ]]                                                      
                                                      
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 

set.number = true
set.tabstop = 4
set.shiftwidth = 4
set.softtabstop = 0
set.expandtab = true
set.swapfile = false
set.relativenumber = true
set.termguicolors = true
set.incsearch = true
set.ruler = true
set.backup = false
set.ignorecase = true
set.hlsearch  = true
set.fileencoding = "utf-8"
set.mouse = "a"
set.splitbelow = true
set.splitright = true
set.undodir = '/home/utkarsh/.vim/undodir' 
set.undofile = true
set.smartindent = true
set.smartcase = true
set.writebackup = false
set.updatetime = 300
set.signcolumn = "yes"

----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--[[
 _____   _      _    _   _____  _____  _   _       _____  ______  _______  _    _  _____  
 |  __ \ | |    | |  | | / ____||_   _|| \ | |     / ____||  ____||__   __|| |  | ||  __ \ 
 | |__) || |    | |  | || |  __   | |  |  \| |    | (___  | |__      | |   | |  | || |__) |
 |  ___/ | |    | |  | || | |_ |  | |  | . ` |     \___ \ |  __|     | |   | |  | ||  ___/ 
 | |     | |____| |__| || |__| | _| |_ | |\  |     ____) || |____    | |   | |__| || |     
 |_|     |______|\____/  \_____||_____||_| \_|    |_____/ |______|   |_|    \____/ |_|     
                                                                                                                                                                                   
]]

-------------------------------------------------------------------------------- LUALINE SETUP ----------------------------------------------------------------------------------------------------------------

-- lualine setup

local lualine  = require('lualine')

local colors = {
    blue ='#0723f2',
    --black = '#040808',
    bg = '#202328',
    fg = '#f0f5f5',
    red = '#f20707',
    violet = '#e505ed',
    grey = '#4f4d4f',
    cyan = '#11d6f5',
    orange = '#f25c05',
    yellow = '#eef205',
    green = '#64f205',
    magenta = '#dd0a9e',
}

local conditions = {
    buffer_not_empty = function()
        return vim.fn.empty(vim.fn.expand('%:t')) ~= 1
    end,
    hide_in_width =  function()
        return vim.fn.winwidth(0) > 80
    end,
    check_git_workspace = function()
        local filepath = vim.fn.expand('%:p:h')
        local gitdir = vim.fn.finddir('.git', filepath .. ';')
        return gitdir and #gitdir > 0 and #gitdir < #filepath
    end,
}



local config = {
    options = {
        theme = {
            normal = { c = { fg = colors.fg, bg = colors.bg}},
            inactive = { c = { fg = colors.fg, bg = colors.bg}}
        },
        component_separators = '',
        section_separators = '',
    },

    sections = {
        lualine_a = {},
        lualine_b = {},
        lualine_c = {},
        lualine_x = {},
        lualine_y = {},
        lualine_z = {},
    },

    inactive_sections = {
        lualine_a = {},
        lualine_b = {},
        lualine_c = {},
        lualine_y = {},
        lualine_x = {},
        lualine_z = {},
    },

    --tabline = {},
    --extensions = {},
}

local function in_left(component)
    table.insert(config.sections.lualine_c, component)
end

local function in_right(component)
    table.insert(config.sections.lualine_x, component)
end


in_left {
    function()
        return '▊'
    end,

    color = {fg = colors.magenta },

    padding = {left = 0, right = 1},
}

in_left {

    --mode component
    function()
        return 'ॐ '
    end,

    color = function()

        --auto change color according to neovims mode
        local mode_color = {
            n = colors.red,
            i = colors.green,
            v = colors.blue,
            ['^V'] = colors.blue,
            V = colors.blue,
            c = colors.magenta,
            no = colors.red,
            s = colors.orange,
            s = colors.orange,
            ['^S'] = colors.orange,
            ic = colors.yellow,
            R = colors.viole,
            cv = colors.red,
            ce = colors.red,
            r = colors.cyan,
            rm = colors.cyan,
            ['r?'] = colors.cyan,
            ['!'] = colors.red,
            t = colors.red,
        }
        return { fg = mode_color[vim.fn.mode()] }
    end,
    padding = { right = 1},
}

in_left {
    'filesize',
    cond = conditions.buffer_not_empty,
}

in_left {
    'filename',
    cond = conditions.buffer_not_empty,
    color = { fg = colors.yellow , gui = 'bold'},
}

in_left {'location'}

in_left { 'progress', color = {fg = colors.fg, gui = 'bold'}}

--[[in_left {
    'diagnostics',
    sources = {'nvim_diagnostic'},
    symbols = { error = '🔴', warn = '⚠️', info = '☀'},
}]]
in_left {
  'diagnostics',
  sources = { 'nvim_diagnostic' },
  symbols = { error = '🔴', warn = '⚠️ ', info = '☀ ' },
  --[[diagnostics_color = {
    color_error = { fg = colors.red },
    color_warn = { fg = colors.yellow },
    color_info = { fg = colors.cyan },
  },]]
}


in_left {
    function()
        return '%='
    end
}

--[[in_left {
  -- Lsp server name .
  function()
    local msg = 'No Active Lsp'
    local buf_ft = vim.api.nvim_buf_get_option(0, 'filetype')
    local clients = vim.lsp.get_active_clients()
    if next(clients) == nil then
      return msg
    end
    for _, client in ipairs(clients) do
      local filetypes = client.config.filetypes
      if filetypes and vim.fn.index(filetypes, buf_ft) ~= -1 then
        return client.name
      end
    end
    return msg
  end,
  icon = ' LSP:',
  color = { fg = '#ffffff', gui = 'bold' },
}]]

-- Now lets add component to right section
--
in_right {
    'o:encoding',
    fmt = string.upper,
    cond = conditions.hide_in_width,
    color = { fg = colors.green, gui = 'bold'},
}

in_right {
    'fileformat',
    fmt = string.upper,
    icons_enabled = false,
    color = { fg = colors.green, gui = 'bold'},
}

in_right {
    'branch',
    icon = '',
    color = {gui = 'bold'}, 
}

in_right {
    'diff',
    symbols = { added = '+', modified= '✏  ', removed = '❌ '},
    diff_color = {
        added = { fg = colors.green, gui = 'bold'},
        modified = { fg = colors.orange, gui = 'bold'},
        removed = { fg = colors.red, gui = 'bold'},
    },
    cond = conditions. hide_in_width,
}

in_right {
    function()
        return '▊'
    end,
    color = { fg = colors.magenta},
    padding = {left = 1},
}


--load your config
lualine.setup(config)


------------------------------------------------------------------------------------- NVIM-TREE SETUP ----------------------------------------------------------------------------------


-- Nvim-tree
-- disable netrw at the very start of your init.lua (strongly advised)
global.loaded_netrw = 1
global.loaded_netrwPlugin = 1

require("nvim-tree").setup({
  sort_by = "case_sensitive",
  view = {
    width = 30,
    mappings = {
      list = {
        { key = "u", action = "dir_up" },
      },
    },
  },
  renderer = {
    group_empty = true,
  },
  filters = {
    dotfiles = true,
  },
})

-- Autopairs
require('nvim-autopairs').setup({
  disable_filetype = { "TelescopePrompt" , "vim" },
})


----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--[[
_______  _    _  ______  __  __  ______   _____ 
 |__   __|| |  | ||  ____||  \/  ||  ____| / ____|
    | |   | |__| || |__   | \  / || |__   | (___  
    | |   |  __  ||  __|  | |\/| ||  __|   \___ \ 
    | |   | |  | || |____ | |  | || |____  ____) |
    |_|   |_|  |_||______||_|  |_||______||_____/ 
                                                  
]]                                                
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



-- Gruvbox

--set.background = "dark" -- or "light" for light mode
--cmd([[colorscheme gruvbox]])





--[[One Dark theme 

require('onedark').setup {
    style = 'darker'
}
require('onedark').load()
--]]



-- Dracula

--local dracula = require("dracula")
--dracula.setup({
  -- customize dracula color palette
--  colors = {
--    bg = "#282A36",
--    fg = "#F8F8F2",
--    selection = "#44475A",
--    comment = "#6272A4",
--    red = "#FF5555",
--    orange = "#FFB86C",
--    yellow = "#F1FA8C",
--    green = "#50fa7b",
--    purple = "#BD93F9",
--    cyan = "#8BE9FD",
--    pink = "#FF79C6",
--    bright_red = "#FF6E6E",
--    bright_green = "#69FF94",
--    bright_yellow = "#FFFFA5",
--    bright_blue = "#D6ACFF",
--    bright_magenta = "#FF92DF",
--    bright_cyan = "#A4FFFF",
--    bright_white = "#FFFFFF",
--    menu = "#21222C",
--    visual = "#3E4452",
--    gutter_fg = "#4B5263",
--    nontext = "#3B4048",
--  },
  -- show the '~' characters after the end of buffers
--  show_end_of_buffer = true, -- default false
  -- use transparent background
--  transparent_bg = true, -- default false
  -- set custom lualine background color
--  lualine_bg_color = "#44475a", -- default nil
  -- set italic comment
--  italic_comment = true, -- default false
  -- overrides the default highlights see `:h synIDattr`
--  overrides = {
    -- Examples
    -- NonText = { fg = dracula.colors().white }, -- set NonText fg to white
    -- NvimTreeIndentMarker = { link = "NonText" }, -- link to NonText highlight
    -- Nothing = {} -- clear highlight of Nothing
--  },
--})
-- cmd[[colorscheme dracula]]
--


-- CATPPUCCIN
--colorscheme catppuccin " catppuccin-latte, catppuccin-frappe, catppuccin-macchiato, catppuccin-mocha"
--vim.cmd.colorscheme "catppuccin-macchiato"
--[[
require("catppuccin").setup({
    flavour = "mocha", -- latte, frappe, macchiato, mocha
    background = { -- :h background
        light = "latte",
        dark = "mocha",
    },
    transparent_background = false,
    show_end_of_buffer = false, -- show the '~' characters after the end of buffers
    term_colors = false,
    dim_inactive = {
        enabled = true,
        shade = "dark",
        percentage = 0.50,
    },
    no_italic = true, -- Force no italic
    no_bold = false, -- Force no bold
    styles = {
        comments = { "italic" },
        conditionals = { "italic" },
        loops = {},
        functions = {},
        keywords = {},
        strings = {},
        variables = {},
        numbers = {},
        booleans = {},
        properties = {},
        types = {},
        operators = {},
    },
    color_overrides = {},
    custom_highlights = {},
    integrations = {
        cmp = true,
        gitsigns = true,
        nvimtree = true,
        telescope = true,
        notify = false,
        mini = false,
        -- For more plugins integrations please scroll down (https://github.com/catppuccin/nvim#integrations)
    },
})

cmd.colorscheme "catppuccin"
--]]

-- TOKYONIGHT

--[[
colorscheme tokyonight-night
colorscheme tokyonight-storm
colorscheme tokyonight-day
--colorscheme tokyonight-moon
-- ]]

require("tokyonight").setup({
  -- your configuration comes here
  -- or leave it empty to use the default settings
  style = "night", -- The theme comes in three styles, `storm`, `moon`, a darker variant `night` and `day`
  light_style = "day", -- The theme is used when the background is set to light
  transparent = false, -- Enable this to disable setting the background color
  terminal_colors = true, -- Configure the colors used when opening a `:terminal` in Neovim
  styles = {
    -- Style to be applied to different syntax groups
    -- Value is any valid attr-list value for `:help nvim_set_hl`
    comments = { italic = false },
    keywords = { italic = false },
    functions = {},
    variables = {},
    -- Background styles. Can be "dark", "transparent" or "normal"
    sidebars = "dark", -- style for sidebars, see below
    floats = "dark", -- style for floating windows
  },
  sidebars = { "qf", "help" }, -- Set a darker background on sidebar-like windows. For example: `["qf", "vista_kind", "terminal", "packer"]`
  day_brightness = 0.3, -- Adjusts the brightness of the colors of the **Day** style. Number between 0 and 1, from dull to vibrant colors
  hide_inactive_statusline = false, -- Enabling this option, will hide inactive statuslines and replace them with a thin border instead. Should work with the standard **StatusLine** and **LuaLine**.
  dim_inactive = false, -- dims inactive windows
  lualine_bold = false, -- When `true`, section headers in the lualine theme will be bold

  --- You can override specific color groups to use other groups or a hex color
  --- function will be called with a ColorScheme table
  ---@param colors ColorScheme
  on_colors = function(colors) end,

  --- You can override specific highlights to use other groups or a hex color
  --- function will be called with a Highlights and ColorScheme table
  ---@param highlights Highlights
  ---@param colors ColorScheme
  on_highlights = function(highlights, colors) end,
})

cmd[[colorscheme tokyonight]]
-- 
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------- BUFFERLINE -----------------------------------------------------------------------------------------
--

-- bufferline
require("bufferline").setup{
    options = {
        
        offsets = {
            {
               filetype = "NvimTree",
               text = "File Explorer 🗂",
               highlight = "Directory",
               sperator = true
            }
        },
    }
}

-- toggleterm
require("toggleterm").setup{}

-- COC
global.coc_global_extension = { 'coc-emmet', 'coc-go', 'coc-html', 'coc-eslint', 'coc-git', 'coc-json', 'coc-pyright', 'coc-tsserver', 'coc-prettier', 'coc-yaml'}

-- Dashboard
--
require('dashboard').setup{
    theme = 'doom',

}
--[[
local db=require'dashboard'
db.setup({
  theme = 'doom',
  config = {
    header = {}, --your header
    center = {
      {
        icon = ' ',
        icon_hl = 'Title',
        desc = 'Find File           ',
        desc_hl = 'String',
        key = 'b',
        keymap = 'SPC f f',
        key_hl = 'Number',
        action = 'lua print(2)'
      },
      {
        icon = ' ',
        desc = 'Find Dotfiles',
        key = 'f',
        keymap = 'SPC f d',
        action = 'lua print(3)'
      },
    },
    footer = {}  --your footer
  }
}) 
]] 


----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 
--[[
_  __ ______ __     __ __  __            _____    _____ 
 | |/ /|  ____|\ \   / /|  \/  |    /\    |  __ \  / ____|
 | ' / | |__    \ \_/ / | \  / |   /  \   | |__) || (___  
 |  <  |  __|    \   /  | |\/| |  / /\ \  |  ___/  \___ \ 
 | . \ | |____    | |   | |  | | / ____ \ | |      ____) |
 |_|\_\|______|   |_|   |_|  |_|/_/    \_\|_|     |_____/ 
                                                          
 ]]                                                       
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


global.mapleader = " "


keymap.set('n', '<leader>w', ':w<CR>',{noremap = true})
keymap.set('i', '<leader>e', '<esc>:w<CR>',{noremap = true})
keymap.set('n', '<leader>q', ':q!<CR>',{noremap = true})
keymap.set('n', '<leader>s', ':so %<CR>',{noremap = true})
keymap.set('n', '<leader>ev', ':vsplit $MYVIMRC<CR>',{noremap = true})
keymap.set('n', '<leader>sv', ':w<CR>:so %<CR>:q<CR>',{noremap = true})
keymap.set('n', '<leader>b', ':NvimTreeToggle<CR>',{noremap = true})

--move around diffrent panes
keymap.set('n', '<A-h>', '<C-w>h',{noremap = true})
keymap.set('n', '<A-j>', '<C-w>j',{noremap = true})
keymap.set('n', '<A-k>', '<C-w>k',{noremap = true})
keymap.set('n', '<A-l>', '<C-w>l',{noremap = true})

--split screen
keymap.set('n', '<leader>h', '<C-w>s',{noremap = true})
keymap.set('n', '<leader>v', '<C-w>v',{noremap = true})

-- resize pane
keymap.set('n', '<leader><Left>', ':vertical resize -10<CR>',{noremap = true})
keymap.set('n', '<leader><Right>',':vertical resize +10<CR>',{noremap = true})
keymap.set('n', '<leader><Up>',   ':resize +10<CR>',{noremap = true})
keymap.set('n', '<leader><Down>', ':resize -10<CR>',{noremap = true})

-- terminal and Floaterm and toggleterm
keymap.set('t', '<esc>', '<C-\\><C-n>',{noremap = true})
keymap.set('n', '<C-n>', ':FloatermToggle<CR>', {noremap = true})



-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--[[

_____   _      _    _   _____  _____  _   _              _____   ______           
 |  __ \ | |    | |  | | / ____||_   _|| \ | |      /\    |  __ \ |  ____|    /\    
 | |__) || |    | |  | || |  __   | |  |  \| |     /  \   | |__) || |__      /  \   
 |  ___/ | |    | |  | || | |_ |  | |  | . ` |    / /\ \  |  _  / |  __|    / /\ \  
 | |     | |____| |__| || |__| | _| |_ | |\  |   / ____ \ | | \ \ | |____  / ____ \ 
 |_|     |______|\____/  \_____||_____||_| \_|  /_/    \_\|_|  \_\|______|/_/    \_\
                                                                                    
]]                                                                                 
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

--- Plugin - packer
return require('packer').startup(function()
  use 'wbthomason/packer.nvim'
  use 'tpope/vim-fugitive'
  use {
    'nvim-lualine/lualine.nvim',
    requires = { 'kyazdani42/nvim-web-devicons', opt = true }
  }
  use 'tiagofumo/vim-nerdtree-syntax-highlight'
  use 'kyazdani42/nvim-web-devicons'
  use 'vim-airline/vim-airline'
  use 'vim-airline/vim-airline-themes'
  
  -- Theme
  use 'navarasu/onedark.nvim'
  use 'Mofiqul/dracula.nvim'
  use { "ellisonleao/gruvbox.nvim" }
  use { "catppuccin/nvim", as = "catppuccin" }
  use 'folke/tokyonight.nvim'
  use 'rmehri01/onenord.nvim'
  use({
  "folke/styler.nvim",
  config = function()
    require("styler").setup({
      themes = {
        markdown = { colorscheme = "gruvbox" },
        help = { colorscheme = "catppuccin-mocha", background = "dark" },
      },
    })
  end,
})

  
  -- Terminal
  use 'voldikss/vim-floaterm'
  use {"akinsho/toggleterm.nvim", tag = '*', config = function()
  require("toggleterm").setup()
end}
  
  -- AutoCompletion and LSP 
  use {'neoclide/coc.nvim', branch = 'release'}

  -- Autopairs
  use {
	"windwp/nvim-autopairs",
    config = function() require("nvim-autopairs").setup {} end
  }

 -- tabline
  use {'akinsho/bufferline.nvim', tag = "v3.*", requires = 'nvim-tree/nvim-web-devicons'}

 -- Nvim-tree
 use {
  'nvim-tree/nvim-tree.lua',
  requires = {
    'nvim-tree/nvim-web-devicons', -- optional, for file icons
  },
  tag = 'nightly' 
}

  -- Dashboard
  use {
  'glepnir/dashboard-nvim',
  event = 'VimEnter',
  config = function()
  require('dashboard').setup {
      -- config
    }
  end,
  requires = {'nvim-tree/nvim-web-devicons'}
}

    
  -- Rainbow pair
end)















