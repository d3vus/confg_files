syntax on
set wildmenu
set ruler
set nu
set relativenumber
set noerrorbells
set tabstop=4 softtabstop=4
set shiftwidth=4
set expandtab
set smartindent
set exrc
set guicursor=
set nohlsearch
set hidden
set nowrap
set smartcase
set noswapfile
set nobackup
set undodir=~/.vim/undodir
set undofile
set incsearch
set scrolloff=8
set termguicolors
set noshowmode
set completeopt=menuone,noinsert,noselect
set signcolumn=yes
"set cmdheight=2
set updatetime=50
set shortmess+=c

call plug#begin("~/.vim/plugged")
  " Plugin Section
    Plug 'luochen1990/rainbow'
    Plug 'voldikss/vim-floaterm'
    Plug 'leafgarland/typescript-vim'
    Plug 'scrooloose/nerdtree'
    Plug 'vim-utils/vim-man'
	Plug 'tiagofumo/vim-nerdtree-syntax-highlight'
    Plug 'Xuyuanp/nerdtree-git-plugin'
    Plug 'ryanoasis/vim-devicons'
    Plug 'mbbill/undotree'
	Plug 'gruvbox-community/gruvbox'
    Plug 'dracula/vim'
    Plug 'arcticicestudio/nord-vim'
	Plug 'vim-airline/vim-airline'
    Plug 'vim-airline/vim-airline-themes'
    Plug 'tpope/vim-fugitive'
    Plug 'jiangmiao/auto-pairs'
    Plug 'romgrk/doom-one.vim'
    Plug 'sainnhe/gruvbox-material'
    "Plug 'rrethy/vim-hexokinase', { 'do': 'make hexokinase' }
    " VSCode like minimap in vim
    "Plug 'wfxr/minimap.vim', {'do': ':!cargo install --locked code-minimap'}
    
    "Python
    Plug 'jeetsukumaran/vim-pythonsense'
    Plug 'Vimjas/vim-python-pep8-indent'
    Plug 'w0rp/ale'
    Plug 'sheerun/vim-polyglot'
    Plug 'neoclide/coc.nvim', {'branch': 'release'}

    "more
    Plug 'machakann/vim-highlightedyank'
    Plug 'tmhedberg/SimpylFold'


    "Themes
    Plug 'sjl/badwolf'
    Plug 'nanotech/jellybeans.vim'

call plug#end()

" Redefine mapLeader key
let mapleader=","

au ColorScheme * hi Normal ctermbg=none guibg=none
autocmd VimEnter * ++nested colorscheme gruvbox

"set background=dark

let &t_SI = "\<esc>[5 q"  " blinking I-beam in insert mode
let &t_SR = "\<esc>[5 q"  " blinking underline in replace mode
let &t_EI = "\<esc>[ q"  " default cursor (usually blinking block) otherwise

" UndoTree
nnoremap <leader>u :UndotreeShow<CR>

"if (has("termguicolors"))
set termguicolors
"endif
syntax enable


" NerdTree Config
let g:NERDTreeShowHidden = 1
let g:NERDTreeMinimalUI = 1
let g:NERDTreeIgnore = []
let g:NERDTreeStatusline = ''
" Automaticaly close nvim if NERDTree is only thing left open
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif
" Toggle
nnoremap <silent> <C-b> :NERDTreeToggle<CR>


" open new split panes to right and below
set splitright
set splitbelow
" turn terminal to normal mode with escape
tnoremap <Esc> <C-\><C-n>
" start terminal in insert mode
au BufEnter * if &buftype == 'terminal' | :startinsert | endif
" open terminal on ctrl+n
function! OpenTerminal()
  split term://fish
  resize 10
endfunction
nnoremap <c-n> :call OpenTerminal()<CR>


" NERDTree Syntax Highlight
" " Enables folder icon highlighting using exact match
let g:NERDTreeHighlightFolders = 1
" " Highlights the folder name
let g:NERDTreeHighlightFoldersFullName = 1
" " Color customization
let s:brown = "905532"
let s:aqua =  "3AFFDB"
let s:blue = "689FB6"
let s:darkBlue = "44788E"
let s:purple = "834F79"
let s:lightPurple = "834F79"
let s:red = "AE403F"
let s:beige = "F5C06F"
let s:yellow = "F09F17"
let s:orange = "D4843E"
let s:darkOrange = "F16529"
let s:pink = "CB6F6F"
let s:salmon = "EE6E73"
let s:green = "8FAA54"
let s:lightGreen = "31B53E"
let s:white = "FFFFFF"
let s:rspec_red = 'FE405F'
let s:git_orange = 'F54D27'
" " This line is needed to avoid error
let g:NERDTreeExtensionHighlightColor = {}
" " Sets the color of css files to blue
let g:NERDTreeExtensionHighlightColor['css'] = s:blue
" " This line is needed to avoid error
let g:NERDTreeExactMatchHighlightColor = {}
" " Sets the color for .gitignore files
let g:NERDTreeExactMatchHighlightColor['.gitignore'] = s:git_orange
" " This line is needed to avoid error
let g:NERDTreePatternMatchHighlightColor = {}
" " Sets the color for file ending with _spec.rb
let g:NERDTreePatternMatchHighlightColor['.*_spec\.rb$'] =  s:rspec_red
" " Sets the color for folders that did not match any rule
let g:WebDevIconsDefaultFolderSymbolColor = s:beige
" " Sets the color for files that did not match any rule
let g:WebDevIconsDefaultFileSymbolColor = s:blue

" NERDTree Git Plugin
let g:NERDTreeGitStatusIndicatorMapCustom = {
    \ "Modified"  : "✹",
    \ "Staged"    : "✚",
    \ "Untracked" : "✭",
    \ "Renamed"   : "➜",
    \ "Unmerged"  : "═",
    \ "Deleted"   : "✖",
    \ "Dirty"     : "✗",
    \ "Clean"     : "✔︎",
    \ 'Ignored'   : '☒',
    \ "Unknown"   : "?",
    \ }


nnoremap <c-n> :call OpenTerminal()<CR>
" use alt+hjkl to move between split/vsplit panls
tnoremap <A-h> <C-\><C-n><C-w>h
tnoremap <A-j> <C-\><C-n><C-w>j
tnoremap <A-k> <C-\><C-n><C-w>k
tnoremap <A-l> <C-\><C-n><C-w>l
nnoremap <A-h> <C-w>h
nnoremap <A-j> <C-w>j
nnoremap <A-k> <C-w>k
nnoremap <A-l> <C-w>l

" Airline-Theme
let g:airline_theme = "gruvbox"
let g:airline_powerline_fonts = 1
let g:airline#extensions#tabline#enabled = 1

" FzF
nnoremap <C-p> :FZF<CR>
let g:fzf_action = {
  \ 'ctrl-t': 'tab split',
  \ 'ctrl-s': 'split',
  \ 'ctrl-v': 'vsplit'
  \}
let g:fzf_preview_window = 'right:50%'
let g:fzf_layout = { 'window': { 'width': 0.9, 'height': 0.6  }  }

"Coc
let g:coc_global_extensions = ['coc-eslint','coc-emmet', 'coc-html', 'coc-json', 'coc-tsserver','coc-pyright','coc-clangd','coc-git','coc-prettier','coc-java']

let g:rainbow_active = 1


"Yank Area
hi HighlightedyankRegion cterm=reverse gui=reverse
let g:highlightedyank_highlight_duration = 1000

"Ale
let g:ale_linters = {'python': '','javascript':'eslint'}
"let g:ale_fixers = {'python': ['black', 'yapf']}

"Additionl settings
"let g:ale_fixers = {'python': ['black', 'yapf', 'remove_trailing_lines', 'trim_whitespace']}

let g:ale_lsp_suggestions = 1
let g:ale_fix_on_save = 1
let g:ale_go_gofmt_options = '-s'
let g:ale_go_gometalinter_options = '— enable=gosimple — enable=staticcheck'
let g:ale_completion_enabled = 0
let g:ale_echo_msg_error_str = 'E'
let g:ale_echo_msg_warning_str = 'W'
let g:ale_echo_msg_format = '[%linter%] [%severity%] %code: %%s'

"Floating terminal
nnoremap <c-t> :FloatermToggle<CR>
" Configuration example

" Set floaterm window's background to black
hi Floaterm guibg=black
" Set floating window border line color to cyan, and background to orange
hi Floaterm guifg=orange

let g:floaterm_shell ="fish"

