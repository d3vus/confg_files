## Tmux 🖥️

#### What’s tmux?
> tmux’s authors describe it as a terminal multiplexer. Behind this fancy term hides a simple concept: Within one terminal window you can open multiple windows and split-views (called “panes” in tmux lingo). Each pane will contain its own, independently running terminal instance. This allows you to have multiple terminal commands and applications running visually next to each other without the need to open multiple terminal emulator windows.

#### How to install tmux

Usualy tmux is pre-installed in most of the Linux distributions but if it is not then install using these instructions:

##### Ubuntu 22.04

`sudo apt install tmux`

##### Fedora 36

`sudo dnf install tmux`

##### Arch-linux

`sudo pacman -S tmux`


#### How to start and setup tmux

> Type 'tmux' in terminal session and Boom 💥 you are inside a tmux session, for better control of tmux sessions, windows and panes inside tmux you can use pre-defined key-bindings for tmux sessions you can find them [here](https://tmuxcheatsheet.com/) and more you can customize your tmux keybindings by pasting this [.tmux.conf](https://gitlab.com/d3vus/confg_files/-/blob/main/Tmux/.tmux.conf) file in your home directory 
(`cp .tmux.conf ~`).


