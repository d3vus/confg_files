## Starship 🚀

### What is starship 

Starship is an extermely customizable prompt means you can make your terminal prompt according to your taste 😁. Its written in rust and as it suggests you can expect a power and speed on your fingerprints.

According to official starship [website](https://starship.rs/) 

> The minimal, blazing-fast, and infinitely customizable prompt for any shell!

Its features are :

1. Compatibility First
   
   > Works on the most common shells on the most common operating systems. Use it everywhere!

2. Rust-Powered

    > Brings the best-in-class speed and safety of Rust, to make your prompt as quick and reliable as possible.

3. Customizable

    > Every little detail is customizable to your liking, to make this prompt as minimal or feature-rich as you'd like it to be.

### How to install starship

For most of the part the latest distributions of Ubuntu , Fedora and Arch-linux provides installation of starship as their native packages but if you are using older distros then you can install rust and use its package manager **cargo** for installation.

#### Ubuntu 22.04

`sudo apt install starship`

#### Fedora 36

`sudo dnf install starship`

#### Arch-Linux

`sudo pacman -S starship`

#### For installation using cargo

`cargo install starship`

### How to setup Starship

You have to tinker your shell script to make your shell starship as its prompt

So for 

1. BASH
    
    Paste this one liner in your ~/.bashrc
    > eval "$(starship init bash)"

2. ZSH

    Paste this one liner in your ~/.zshrc
    > eval "$(starship init zsh)"

3. FISH

    Paste this one liner in your ~/.config/fish/config.fish
    > starship init fish | source

Now you have setup starship in your shell, you can also configure according to your taste for that paste this **starship.toml** file in **/home/username/.config** directory.
