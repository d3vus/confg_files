
;; If you are distributing this theme, please replace this comment
;; with the appropriate license attributing the original VS Code
;; theme author.

(deftheme beardedtheme-featwill "A nice  theme.")


(let (
(color0 "#14111f")
(color1 "#bdb6d3")
(color2 "#00000b")
(color3 "#b498f5")
(color4 "#fce38a")
(color5 "#000000")
(color6 "#000006")
(color7 "#a9a2bf")
(color8 "#958eab")
(color9 "#585775")
(color10 "#8ad0ff")
(color11 "#f7775a")
(color12 "#5fee9b")
(color13 "#44f8e9")
(color14 "#ff8ea0")
(color15 "#37324a")
(color16 "#050210")
(color17 "#aea7c4")
(color18 "#090614")
(color19 "#b2abc8"))


(custom-theme-set-faces
'beardedtheme-featwill


;; BASIC FACES
`(default ((t (:background ,color0 :foreground ,color1 ))))
`(hl-line ((t (:background ,color2 ))))
`(cursor ((t (:background ,color3 :foreground ,color4 ))))
`(region ((t (:background ,color5 ))))
`(secondary-selection ((t (:background ,color6 ))))
`(fringe ((t (:background ,color0 ))))
`(mode-line-inactive ((t (:background ,color2 :foreground ,color7 ))))
`(mode-line ((t (:background ,color5 :foreground ,color8 ))))
`(minibuffer-prompt ((t (:background ,color0 :foreground ,color9 ))))
`(vertical-border ((t (:foreground ,color5 ))))


;; FONT LOCK FACES
`(font-lock-builtin-face ((t (:foreground ,color10 ))))
`(font-lock-comment-face ((t (:fontStyle :italic t ))))
`(font-lock-constant-face ((t (:foreground ,color11 ))))
`(font-lock-function-name-face ((t (:foreground ,color10 ))))
`(font-lock-string-face ((t (:foreground ,color12 ))))
`(font-lock-type-face ((t (:foreground ,color13 ))))
`(font-lock-variable-name-face ((t (:foreground ,color14 ))))


;; linum-mode
`(linum ((t (:foreground ,color15 ))))
`(linum-relative-current-face ((t (:foreground ,color15 ))))


;; display-line-number-mode
`(line-number ((t (:foreground ,color15 ))))
`(line-number-current-line ((t (:foreground ,color15 ))))


;; THIRD PARTY PACKAGE FACES


;; doom-modeline-mode
`(doom-modeline-bar ((t (:background ,color5 :foreground ,color8 ))))
`(doom-modeline-inactive-bar ((t (:background ,color2 :foreground ,color7 ))))


;; web-mode
`(web-mode-string-face ((t (:foreground ,color12 ))))
`(web-mode-html-attr-name-face ((t (:foreground ,color13 ))))


;; company-mode
`(company-tooltip ((t (:background ,color16 :foreground ,color17 ))))


;; org-mode
`(org-block ((t (:background ,color18 :foreground ,color19 ))))))


(custom-theme-set-variables
  'beardedtheme-featwill
  '(linum-format " %3i "))


;;;###autoload
(when load-file-name
  (add-to-list 'custom-theme-load-path
               (file-name-as-directory (file-name-directory load-file-name))))


;;;###autoload
(defun beardedtheme-featwill-theme()
  "Apply the beardedtheme-featwill-theme."
  (interactive)
  (load-theme 'beardedtheme-featwill t))


(provide-theme 'beardedtheme-featwill)


;; Local Variables:
;; no-byte-compile: t
;; End:


;; Generated using https://github.com/nice/themeforge
;; Feel free to remove the above URL and this line.
