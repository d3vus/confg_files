
;; If you are distributing this theme, please replace this comment
;; with the appropriate license attributing the original VS Code
;; theme author.

(deftheme atomize "A nice dark theme.")


(let (
(color0 "#282C34")
(color1 "#ABB2BF")
(color2 "#3c4048")
(color3 "#528BFF")
(color4 "#555961")
(color5 "#3E4452")
(color6 "#35393f")
(color7 "#b1b9c8")
(color8 "#494d53")
(color9 "#c5cddc")
(color10 "#D7DAE0")
(color11 "#73777f")
(color12 "#61AFEF")
(color13 "#5C6370")
(color14 "#D19A66")
(color15 "#C678DD")
(color16 "#98C379")
(color17 "#E06C75")
(color18 "#4B5365")
(color19 "#373b43")
(color20 "#bac1ce")
(color21 "#33373f")
(color22 "#b6bdca"))


(custom-theme-set-faces
'atomize


;; BASIC FACES
`(default ((t (:background ,color0 :foreground ,color1 ))))
`(hl-line ((t (:background ,color2 ))))
`(cursor ((t (:foreground ,color3 ))))
`(region ((t (:background ,color4 ))))
`(secondary-selection ((t (:background ,color5 ))))
`(fringe ((t (:background ,color0 ))))
`(mode-line-inactive ((t (:background ,color6 :foreground ,color7 ))))
`(mode-line ((t (:background ,color8 :foreground ,color9 ))))
`(minibuffer-prompt ((t (:background ,color0 :foreground ,color10 ))))
`(vertical-border ((t (:foreground ,color11 ))))


;; FONT LOCK FACES
`(font-lock-builtin-face ((t (:foreground ,color12 ))))
`(font-lock-comment-face ((t (:foreground ,color13 :fontStyle :italic t ))))
`(font-lock-constant-face ((t (:foreground ,color14 ))))
`(font-lock-function-name-face ((t (:foreground ,color12 ))))
`(font-lock-keyword-face ((t (:foreground ,color15 ))))
`(font-lock-string-face ((t (:foreground ,color16 ))))
`(font-lock-variable-name-face ((t (:foreground ,color17 ))))


;; linum-mode
`(linum ((t (:foreground ,color18 ))))
`(linum-relative-current-face ((t (:foreground ,color18 ))))


;; display-line-number-mode
`(line-number ((t (:foreground ,color18 ))))
`(line-number-current-line ((t (:foreground ,color18 ))))


;; THIRD PARTY PACKAGE FACES


;; doom-modeline-mode
`(doom-modeline-bar ((t (:background ,color8 :foreground ,color9 ))))
`(doom-modeline-inactive-bar ((t (:background ,color6 :foreground ,color7 ))))


;; web-mode
`(web-mode-string-face ((t (:foreground ,color16 ))))
`(web-mode-html-tag-face ((t (:foreground ,color15 ))))
`(web-mode-html-tag-bracket-face ((t (:foreground ,color15 ))))


;; company-mode
`(company-tooltip ((t (:background ,color19 :foreground ,color20 ))))


;; org-mode
`(org-block ((t (:background ,color21 :foreground ,color22 ))))
`(org-block-begin-line ((t (:foreground ,color13 ))))))


(custom-theme-set-variables
  'atomize
  '(linum-format " %3i "))


;;;###autoload
(when load-file-name
  (add-to-list 'custom-theme-load-path
               (file-name-as-directory (file-name-directory load-file-name))))


;;;###autoload
(defun atomize-theme()
  "Apply the atomize-theme."
  (interactive)
  (load-theme 'atomize t))


(provide-theme 'atomize)


;; Local Variables:
;; no-byte-compile: t
;; End:


;; Generated using https://github.com/nice/themeforge
;; Feel free to remove the above URL and this line.
