## How to setup alacritty in your linux environment

### For Ubuntu 22.04
```
sudo apt update 
sudo apt install alacritty 
```

### For Fedora 36

``` 
sudo dnf update
sudo dnf install alacritty
```

### For Arch

```
sudo pacman -Syu
sudo pacman -S alacritty
```
